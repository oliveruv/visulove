// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "ofxMidi.h"

typedef struct InputData {
        ofPoint object_point;
        ofPoint effect_point;

        float object_spread;
        float effect_spread;
        float hand_spread;

        float object_x_norm;
        float object_y_norm;
        float object_z_norm;
        float effect_y_norm;
        float effect_x_norm;
        float effect_z_norm;

        std::vector<ofxMidiMessage> midi_messages;

} InputData;

