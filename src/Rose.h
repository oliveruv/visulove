// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "InputData.h"
#include "VisuUtilities.h"
#include "TransitionType.h"

class Rose {
    public:
        Rose(){}
        // Each Rose must delete any other Roses it references.
        // It should also delete any ofFbo it has allocated.
        virtual ~Rose() {}

        // Each Rose is responsible for taking whatever Rose came before it,
        // and including it in itself somehow. Each Rose must:
        //  - Check if this is null, and respond accordingly.
        //    (Store a pointer to it if you want to use it.)
        //  - delete the Rose once it has finished using it.
        //  - delete all children in its destructor.
        //  - pass on data and call update/draw if it wants to
        //    use the Rose in some way.
        virtual void setup(Rose* first_child, int output_width, int output_height) = 0;
        virtual void draw(InputData& l) = 0;
        virtual void draw_settings(InputData& l, int setting_width, int setting_height) {}
        virtual void update(InputData& l) = 0;

        // The Rose has to declare its name and its author's name. The names
        // MUST contain only alpha-numeric characters (A-Z, a-z, 0-9), dashes
        // and underscores (- and _).
        //
        // These two must be unique for each Rose when they are combined.
        // They must be equal to the subdirectories under VisuLove/roses/
        // that this Rose is to be placed under. For example, a Rose located
        // at VisuLove/roses/oliveruv/StarRose/ MUST return oliveruv for
        // author_name and StarRose for rose_name.
        //
        // This is essential for data lookups and for indexing Roses on the web.
        // TODO: Expose all this through a RoseMetadata struct instead!
        // (also non-virtual static functions that are overloaded, where the
        // original one throws an error telling folks to implement)
        virtual string author_name() = 0;
        virtual string rose_name() = 0;

        // If the next Rose uses the WRAPPED transition, when it is time
        // to start the handover animation, do_transition is called
        // with the next Rose as argument.
        //
        // When the handover animation is done, ensure that
        // wants_handover returns true and that rose_to_handover returns
        // the Rose to hand over to (normally the same rose given as
        // argument to do_transition.)
        virtual void do_transition(TransitionType t, Rose* next_rose);
        virtual bool wants_handover();
        virtual Rose *rose_to_promote();

        // To ease Rose construction, you should load shaders from file and
        // make this command reload them. This way, you can easily adjust
        // things on the fly without having to recompile the whole shebang in
        // between iteration. Also be nice and call reload_shaders on any child
        // Roses your Rose is using.
        //
        // When compiling, shaders are copied to VisuLove/bin/data/roses/...
        // and it is from here that the shaders will be loaded at runtime.
        //
        // If you want to live-edit shaders from their src/roses/ directory,
        // set DEBUG=true.
        virtual void reload_shaders() {}

        // These functions are called on the top rose when certain cofigurable
        // keys are pressed. They can be used to do anything you want! They will
        // be called during update(), before the Rose's update() is called.
        virtual void alpha() {}
        virtual void beta() {}
        virtual void gamma() {}

        virtual void key_pressed(int key) {}

    protected:
        int output_width;
        int output_height;
        bool wants_handover_;
        Rose *handover_to;

        // Used by the Rose itself to refer to its private data.
        string data_ref(string data_name) {
            string data_path = "roses/" + author_name() + '/' + rose_name() + '/' + data_name;
            if (DEBUG) {
                // Use data from the source directory instead of from the bin/data directory.
                data_path = "../../../src/roses/" + author_name() + '/' + rose_name() + '/' + data_name;
            }
            return data_path;
        }
};
