// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "RoseCreator.h"
#include "TransitionType.h"

class VisuPlaylist {

    virtual RoseCreator start() = 0;

    virtual RoseCreator next_rose() = 0;
    virtual RoseCreator previous_rose() = 0;

    virtual TransitionType peek_transition() = 0;
};
