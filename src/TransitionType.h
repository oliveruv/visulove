// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

// A TransitionType specifies how to visually transition out
// of a given Rose to whichever Rose comes next.

enum TransitionType {
    WRAPPING,
    WRAPPED
};

