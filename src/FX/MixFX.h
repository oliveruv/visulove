// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "ofxFXObject.h"
#include "VisuUtilities.h"
#include "Settings.h"

class MixFX : public ofxFXObject {
public:
    float kaleido_speed;
    float kaleido_sides;
    float kaleido_opacity;

    float fade_value;
    float saturation;
    float contrast;
    float brightness;
    float opacity;
    float red;
    float green;
    float blue;

    bool should_strobe;
    float strobe_red;
    float strobe_blue;
    float strobe_green;
    float strobe_opacity;
    float strobe_frequency;

    MixFX(){
        passes = 1;
        internalFormat = GL_RGBA;
        load("shaders/mix_shader");

        last_timestamp = 0.0;

        kaleido_time = 0.0;
        kaleido_speed = 0.5;
        kaleido_sides = 0.0;
        kaleido_opacity = 0.0;

        fade_value = 0.5;
        opacity = 1.0;
        saturation = 1.0;
        contrast = 0.0;
        brightness = 1.0;
        red = 1.0;
        green = 1.0;
        blue = 1.0;

        should_strobe = false;
        time_since_flash = 1000.0;
        strobe_red = 1.0;
        strobe_blue = 1.0;
        strobe_green = 1.0;
        strobe_opacity = 1.0;
        strobe_frequency = 1.0;
        max_strobe_rate = 2 / static_cast<float>(Settings::get_int("output:fps"));
    }

protected:
    float last_timestamp;

    float kaleido_time;

    float max_strobe_rate;
    float time_since_flash;

    void injectUniforms()
    {
        float new_time = ofGetElapsedTimef();
        float time_diff = new_time - last_timestamp;

        kaleido_time += (0.5 - kaleido_speed) * time_diff;
        last_timestamp = new_time;

        shader.setUniform1f("kaleido_time", kaleido_time);
        shader.setUniform1f("kaleido_sides", 1 + kaleido_sides * 19.0);
        shader.setUniform1f("kaleido_opacity", kaleido_opacity);

        shader.setUniform1f("fade_value", fade_value);
        shader.setUniform1f("saturation", saturation);
        shader.setUniform1f("contrast", contrast);
        shader.setUniform1f("brightness", brightness);
        shader.setUniform1f("opacity", opacity);
        shader.setUniform1f("red", red);
        shader.setUniform1f("green", green);
        shader.setUniform1f("blue", blue);

        time_since_flash += time_diff;

        if (should_strobe && max_strobe_rate * (1 / strobe_frequency) <= time_since_flash) {
            time_since_flash = 0.0;
            shader.setUniform1f("strobe_red", strobe_red);
            shader.setUniform1f("strobe_blue", strobe_blue);
            shader.setUniform1f("strobe_green", strobe_green);
            shader.setUniform1f("strobe_opacity", strobe_opacity);
        } else {
            shader.setUniform1f("strobe_opacity", 0.0);
        }
    }
};
