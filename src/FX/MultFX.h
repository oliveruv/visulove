// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include <algorithm>

#include "ofMain.h"
#include "ofxFXObject.h"
#include "VisuUtilities.h"

class MultFX : public ofxFXObject {
public:
    MultFX(){
        passes = 1;
        internalFormat = GL_RGBA;
        load("shaders/mult_shader");
    }

    // All inputs are blended together with multiplication.

    // Prominence tells us how much each input will be visible. The
    // input(s) with the largest prominence will be entirely included in
    // the multiplication, the others will have their efficacy reduced
    // as a fraction of the maximum prominence.

    // This gives us fine-grained control over the blending of as many
    // inputs as we like.

    // Assume two inputs, then prominences: 1 and 0.5 will give the same
    // result as the prominences 0.5 and 0.25.

    float prominence_0 = 1.0;
    float prominence_1 = 1.0;

    bool invert[2] = {false, false};
    bool transp[2] = {false, false};
    bool greysc[2] = {false, false};

protected:
    void injectUniforms()
    {
        float max_prominence = std::max(prominence_0, prominence_1);
        if (max_prominence == 0) {
            // The user accidentally set all prominences to zero
            shader.setUniform1f("prominence_0", 1);
            shader.setUniform1f("prominence_1", 1);
        } else {
            float mod = 1 / max_prominence;
            shader.setUniform1f("prominence_0", prominence_0 * mod);
            shader.setUniform1f("prominence_1", prominence_1 * mod);
        }

        if (greysc[0]) {
            shader.setUniform1f("greysc_0", 1.0);
        } else {
            shader.setUniform1f("greysc_0", 0.0);
        }

        if (greysc[1]) {
            shader.setUniform1f("greysc_1", 1.0);
        } else {
            shader.setUniform1f("greysc_1", 0.0);
        }

        if (transp[0]) {
            shader.setUniform1f("transp_0", 1.0);
        } else {
            shader.setUniform1f("transp_0", 0.0);
        }

        if (transp[1]) {
            shader.setUniform1f("transp_1", 1.0);
        } else {
            shader.setUniform1f("transp_1", 0.0);
        }

        if (invert[0]) {
            shader.setUniform1f("invert_0", 1.0);
        } else {
            shader.setUniform1f("invert_0", 0.0);
        }

        if (invert[1]) {
            shader.setUniform1f("invert_1", 1.0);
        } else {
            shader.setUniform1f("invert_1", 0.0);
        }
    }
};
