// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include <memory>
#include <string>

#include "ofxXmlSettings.h"

// CLEAN should not be a singleton

class Settings final {
public:
    ~Settings();

    static int get_int(std::string id, int default_value = 0);
    static bool get_bool(std::string id, bool default_value = false);
    static std::string get_string(
            std::string id,
            std::string default_value = "");

private:
    Settings();

    Settings(const Settings &) = delete;
    Settings(Settings &&) = delete;
    Settings & operator = (const Settings &) = delete;
    Settings & operator = (Settings &&) = delete;

    static Settings & instance();
    static void warn_if_unset(std::string id);

    ofxXmlSettings s_;
};
