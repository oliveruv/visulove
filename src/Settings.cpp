// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include <algorithm>
#include <string>

#include "Settings.h"

Settings::Settings()
{
    if(!s_.loadFile("visulove_settings.xml")){
        ofLog(OF_LOG_ERROR, "Cannot load settings file");
    }
}

Settings::~Settings()
{
}

bool Settings::get_bool(std::string id, bool default_value)
{
    warn_if_unset(id);
    // TODO take strings, gotta convert from getValue's return
    // type CString to c++ string

    int default_val = 0;
    if (default_value == true) {
        default_val = 1;
    }

    int val = instance().s_.getValue("settings:" + id, default_val);
    /* val = std::transform(val.begin(), val.end(), ::tolower); */
    /* if (val == "true") { */
    /*     return true; */
    /* } */

    if (val == 1) {
        return true;
    }

    return false;
}

int Settings::get_int(std::string id, int default_value)
{
    warn_if_unset(id);
    return instance().s_.getValue("settings:" + id, default_value);
}

std::string Settings::get_string(std::string id, std::string default_value)
{
    warn_if_unset(id);
    return instance().s_.getValue("settings:" + id, default_value);
}

Settings & Settings::instance()
{
    static Settings instance;
    return instance;
}

void Settings::warn_if_unset(std::string id)
{
    // TODO this breaks lots of stuff :v
    /* bool is_set = instance().s_.tagExists(id); */
    /* if (!is_set) { */
    /*     ofLog(OF_LOG_WARNING, "Setting not set: %s", id.c_str()); */
    /* } */
}
