// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#version 150

// these are for the programmable pipeline system
uniform mat4 modelViewProjectionMatrix;
in vec4 position;
out float distNorm;

uniform float gravity_range;
uniform vec2 star_pos;

void main()
{
    // copy position so we can work with it.
    vec4 pos = position;

    // direction vector from hole/star position to vertex position.
    vec2 dir = pos.xy - star_pos;

    // distance between the mouse position and vertex position.
    float dist =  sqrt(dir.x * dir.x + dir.y * dir.y);

    // normalise distance between 0 and 1.
    distNorm = dist / gravity_range;

    // flip it so the closer we are the greater the repulsion.
    distNorm = 1.0 - distNorm;

    // make the direction vector magnitude fade out the further it gets from mouse position.
    dir *= distNorm * distNorm;

    // remove the direction vector from the vertex position.
    pos.x -= dir.x;
    pos.y -= dir.y;

    // finally set the pos to be that actual position rendered
    gl_Position = modelViewProjectionMatrix * pos;
}
