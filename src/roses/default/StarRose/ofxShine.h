// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "ofxFXObject.h"
#include "VisuUtilities.h"

class ofxShine : public ofxFXObject {
public:
    ofxShine(){
        passes = 2;
        internalFormat = GL_RGBA;

        gl3FragmentShader = "#version 150\n";
        gl3FragmentShader += STRINGIFY(
                uniform sampler2DRect backbuffer;
                out vec4 outputColor;

                void main(){
                    vec4 sum = vec4(0);
                    vec2 st = gl_FragCoord.st;
                    int j;
                    int i;
                    for(i=-4; i<=4; i++){
                        for (j=-4; j<=4; j++){
                            sum += texture(backbuffer, st + vec2(j, i)*0.004) * 0.0123457;
                        }
                    }

                    outputColor = texture(backbuffer, st) + sum * 0.5;
                });
    }
};
