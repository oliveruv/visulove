// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "ofxShine.h"
#include "ofFbo.h"

#include "Rose.h"
#include "InputData.h"

//--------------------------------------------------------------
// STARS!
//
// Both hands:
//      mid-point between hands is position of star
//      distance between hands controls star size
//
// Object hand:
//      z position controls opacity of lines
//      y position controls fadeout of lines
//      finger spread controls line density
// 
// Effect hand:
//      x position and finger spread control line flickering
//      y position controls line color
//      z position controls star plane rotation speed

class StarRose : public Rose {

    public:
        void setup(Rose* first_child, int output_width, int output_height);
        void update(InputData& l);
        void draw(InputData& l);

        void reload_shaders();

        string author_name() {return "default";}
        string rose_name() {return "StarRose";}

    private:
        ofPlanePrimitive plane;
        ofPoint star_position;

        ofFbo render_buffer;
        ofShader star_shader;
        ofxShine ofx_shine;

        static const int PLANE_DENSITY_BASE = 80;
        int plane_grid_size;
        int plane_width;
        int plane_height;
        int plane_columns;
        int plane_rows;
        float summed_plane_rotation;

        //Assumes density is [0,1]
        void set_plane_density(float density);
};
