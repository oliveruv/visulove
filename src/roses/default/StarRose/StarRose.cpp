// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include "StarRose.h"
#include "VisuUtilities.h"

//Assumes density is [0,1]
void StarRose::set_plane_density(float density) {

    plane_grid_size = PLANE_DENSITY_BASE * ofMap(density, 0, 1, 0.25, 1);

    plane_columns = plane_width / plane_grid_size;
    plane_rows = plane_height / plane_grid_size;

    plane.set(plane_width, plane_height, plane_columns, plane_rows, OF_PRIMITIVE_TRIANGLES);
}

void StarRose::setup(Rose* first_child, int output_width, int output_height) {
    this->output_width = output_width;
    this->output_height = output_height;
    if (first_child)
        delete first_child;

    summed_plane_rotation = 0;

    star_shader.load(data_ref("star_shader"));

    // We enlarge the plane so it will always cover the entire screen.
    plane_width = output_width * 6;
    plane_height = output_height * 6;
    plane_grid_size = PLANE_DENSITY_BASE;
    plane_columns = plane_width / plane_grid_size;
    plane_rows = plane_height / plane_grid_size;

    plane.set(plane_width, plane_height, plane_columns, plane_rows, OF_PRIMITIVE_TRIANGLES);

    render_buffer.allocate(output_width, output_height, GL_RGBA);
    ofx_shine.allocate(output_width, output_height, GL_RGBA);

    render_buffer.begin();
    ofClear(0,255);
    render_buffer.end();
}

void StarRose::update(InputData& l) {
    set_plane_density(1 - l.object_spread);

    // Set up values for rotating input in the same way that we have rotated the plane.
    float plane_rotation = 5 * (1 - l.effect_z_norm);
    summed_plane_rotation += plane_rotation;
    if (summed_plane_rotation > 360)
        summed_plane_rotation -= 360;

    // rotate the star-plane
    plane.rotate(plane_rotation, ofVec3f(0, 0, 1));

    // Position of star is between the hands.
    star_position.x = (l.object_point.x + l.effect_point.x) / 2;
    star_position.y = (l.object_point.y + l.effect_point.y) / 2;
    star_position.z = (l.object_point.z + l.effect_point.z) / 2;
    // Rotate and set star position, note that we have to inverse the rotation!
    // This is probably related to the TODO above, where we have to reverse the
    // y-axis to get the leap -> shader input working properly.
    star_position.rotate(-summed_plane_rotation, ofVec3f(0, 0, 1));
}

void StarRose::draw(InputData& l) {
    ofBackground(1,1,1);
    // Explicitly disable depth test in case another Rose had activated it during its
    // drawing. Otherwise we'll just get a black screen.
    ofDisableDepthTest();
    ofEnableAlphaBlending();

    ofFloatColor c = ofColor::fromHsb(255 * l.effect_y_norm, 255, 255);
    // Set transparency to depend on object hand z position
    c.a = VisuUtilities::smooth_map(1 - l.object_z_norm);
    float line_color[4] = {c.r, c.g, c.b, c.a};

    render_buffer.begin();
    {
        // Fill with transparent black square to fade out previous frames.
        ofPushStyle();
        ofFill();
        /* ofSetColor(0, 0, 0, ofMap(1 - l.object_y_norm, 0, 1, -1, 1) * 5 + 20 ); // TODO: map this to palm angle instead */
        ofSetColor(0, 0, 0, 15);
        ofRect(0, 0, output_width, output_height);
        ofPopStyle();

        star_shader.begin();
        ofPushMatrix();
        {
            star_shader.setUniform2f("star_pos", star_position.x, star_position.y);
            star_shader.setUniform1f("time", ofGetElapsedTimef());
            star_shader.setUniform4fv("line_color", &line_color[0]);
            star_shader.setUniform1f("color_modifier", ofMap(l.effect_spread, 0, 1, 0.2, 15));
            star_shader.setUniform1f("flicker_magnitude", 20 + l.effect_x_norm * 100);
            star_shader.setUniform1f("gravity_range", l.hand_spread * 500);
            // center screen.
            float cx = output_width / 2.0;
            float cy = output_height / 2.0;
            ofTranslate(cx, cy);

            plane.drawWireframe();
        }
        ofPopMatrix();
        star_shader.end();
    }
    render_buffer.end();

    ofx_shine << render_buffer;
    ofx_shine.draw(0, 0, output_width, output_height);
}

void StarRose::reload_shaders() {
    star_shader.unload();
    star_shader.load(data_ref("star_shader"));
}
