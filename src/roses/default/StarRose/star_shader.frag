// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#version 150

in float distNorm;
out vec4 outputColor;

uniform vec4 line_color;
uniform float time;
uniform float flicker_magnitude;
uniform float color_modifier;
 
void main()
{
    vec4 newColor = line_color;
    float color_mod = color_modifier * sin(time*9);
    newColor.r *= smoothstep(-1, 1, sin(gl_FragCoord.x/10 + gl_FragCoord.y/color_mod));
    newColor.g *= smoothstep(-1, 1, sin(gl_FragCoord.x/color_mod - gl_FragCoord.y/10));
    newColor.b *= smoothstep(-1, 1, sin(gl_FragCoord.x/5) * sin(gl_FragCoord.y/5));

    // Adjust depending on how far from the 'star' the fragment is.
    float c = 1.0 - distNorm;
    float alpha_adjust = sin(time * flicker_magnitude + gl_FragCoord.x/10 + gl_FragCoord.y/10);
    float alpha_factor = smoothstep(-1, 1, alpha_adjust);
    newColor.r *= c;
    newColor.g *= c;
    newColor.b *= c;
    newColor.a *= c * alpha_factor;
    outputColor = newColor;
}
