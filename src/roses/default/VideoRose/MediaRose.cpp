// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include "Settings.h"
#include "MediaRose.h"
#include "VisuUtilities.h"

inline bool ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

MediaRose::MediaRose()
{
}

MediaRose::~MediaRose()
{
}

void MediaRose::setup(Rose* first_child, int output_width, int output_height)
{
    this->output_width = output_width;
    this->output_height = output_height;
    wants_handover_ = false;
    handover_to = NULL;
    if (first_child)
        delete first_child;

    // Prepare shaders
    mix_.allocate(output_width, output_height, GL_RGBA);
    mult0_.allocate(output_width, output_height, GL_RGBA);
    mult1_.allocate(output_width, output_height, GL_RGBA);

    // Connect medias to shaders
    media0_.set_mom(&mult0_, 0);
    media1_.set_mom(&mult0_, 1);
    media2_.set_mom(&mult1_, 0);
    media3_.set_mom(&mult1_, 1);

    // Init videos
    // TODO figure out why uncommenting these breaks stuff,
    // but manually enabling the same videos doesn't break
    // anything.
    /* set_media(media0_, video_paths_[0]); */
    /* set_media(media1_, video_paths_[5]); */
    /* set_media(media2_, video_paths_[2]); */
    /* set_media(media3_, video_paths_[3]); */

    selected_media_ = &media0_;
    selected_page_ = 0;
    prev_selected_page_ = 0;
    load_media_list();
}

void fill_dir(std::string dir_path, std::vector<std::string> & file_list)
{
    // Add files
    ofDirectory dir(dir_path);
    dir.allowExt("webm");
    dir.allowExt("mp4");
    dir.listDir();

    for(int i = 0; i < dir.size(); i++){
        file_list.push_back(dir.getPath(i));
    }

    // Recurse
    ofDirectory dir2(dir_path);
    dir2.allowExt("");
    dir2.listDir();

    for(int i = 0; i < dir2.size(); i++){
        fill_dir(dir2.getPath(i), file_list);
    }

}

void MediaRose::load_media_list()
{
    selected_path_ = 0;

    // TODO separate page switching and xml loading
    std::string media_xml_path = Settings::get_string("media");
    media_xml_.loadFile(media_xml_path);
    media_xml_.pushTag("pages");
    n_pages_ = media_xml_.getNumTags("page");

    // TODO error handling
    media_xml_.pushTag("page", selected_page_);

    video_paths_.clear();

    int n_dirs = media_xml_.getNumTags("directory");
    for (int i = 0; i < n_dirs; ++i) {
        std::string dir_path = media_xml_.getValue("directory", "", i);
        fill_dir(dir_path, video_paths_);
    }

    n_paths_ = video_paths_.size();

    prev_selected_page_ = selected_page_;
}

void MediaRose::update(InputData &l)
{
    if (l.midi_messages.size() > 0) {
        for (int i = 0; i < l.midi_messages.size(); ++i) {
            handle_midi(l.midi_messages[i]);
        }
    }

    if (prev_selected_page_ != selected_page_) {
        load_media_list();
    }

    media0_.update();
    media1_.update();
    media2_.update();
    media3_.update();

    bool needs_update = false;
    bool mix_needs_update = false;

    if (media0_.has_new_frame()) {
        mult0_.setTexture(media0_, 0);
        needs_update = true;
    }

    if (media1_.has_new_frame()) {
        mult0_.setTexture(media1_, 1);
        needs_update = true;
    }
    if (needs_update) {
        mult0_.update();
        mix_.setTexture(mult0_.getTextureReference(), 0);
        mix_needs_update = true;
    }

    needs_update = false;

    if (media2_.has_new_frame()) {
        mult1_.setTexture(media2_, 0);
        needs_update = true;
    }
    if (media3_.has_new_frame()) {
        mult1_.setTexture(media3_, 1);
        needs_update = true;
    }

    if (needs_update) {
        mult1_.update();
        mix_.setTexture(mult1_.getTextureReference(), 1);
        mix_needs_update = true;
    }

    if (mix_needs_update) {
        mix_.update();
    }
}

void MediaRose::set_media(Media & media, std::string path)
{
    media.set_source(path);
}

void MediaRose::set_camera(Media & media)
{
    media.set_source_camera();
}

void adjust_level(std::string midi_id, ofxMidiMessage m, bool & value)
{
    // ignore m.channel
    if (Settings::get_int("midi:" + midi_id) == m.control) {
        float n = VU::normalize_midi(m.value);
        if (n < 0.5) {
            value = false;
        } else {
            value = true;
        }
    }
}

void adjust_level(std::string midi_id, ofxMidiMessage m, float & value)
{
    // ignore m.channel
    if (Settings::get_int("midi:" + midi_id) == m.control) {
        value = VU::normalize_midi(m.value);
    }
}

void MediaRose::handle_midi(ofxMidiMessage m)
{
    /* if (m.channel != 1) { */
    /*     return; */
    /* } */

    adjust_level("top_mult:prominence_0", m, mult0_.prominence_0);
    adjust_level("top_mult:prominence_1", m, mult0_.prominence_1);

    adjust_level("bottom_mult:prominence_0", m, mult1_.prominence_0);
    adjust_level("bottom_mult:prominence_1", m, mult1_.prominence_1);

    adjust_level("mix:fade", m, mix_.fade_value);
    adjust_level("mix:saturation", m, mix_.saturation);
    adjust_level("mix:contrast", m, mix_.contrast);
    adjust_level("mix:brightness", m, mix_.brightness);
    adjust_level("mix:opacity", m, mix_.opacity);
    adjust_level("mix:red", m, mix_.red);
    adjust_level("mix:green", m, mix_.green);
    adjust_level("mix:blue", m, mix_.blue);

    adjust_level("mix:kaleido_speed", m, mix_.kaleido_speed);
    adjust_level("mix:kaleido_sides", m, mix_.kaleido_sides);
    adjust_level("mix:kaleido_opacity", m, mix_.kaleido_opacity);

    adjust_level("mix:should_strobe", m, mix_.should_strobe);
    adjust_level("mix:strobe_red", m, mix_.strobe_red);
    adjust_level("mix:strobe_green", m, mix_.strobe_green);
    adjust_level("mix:strobe_blue", m, mix_.strobe_blue);
    adjust_level("mix:strobe_opacity", m, mix_.strobe_opacity);
    adjust_level("mix:strobe_frequency", m, mix_.strobe_frequency);

}

void MediaRose::draw(InputData &l)
{
    mix_.draw(0, 0, output_width, output_height);
}

void drawBoolIndicator(const bool & val, int color, int xshift)
{
    VU::ofPush();
    if (val) {
        ofTranslate(xshift, 0);
        ofSetColor(ofColor::fromHsb(color, 255, 255, 175));
        ofRect(0, 0, 20, 20);
    }
    VU::ofPop();
}

void MediaRose::draw_vid_thumb(Media & media, int x, int y, int w, int h)
{
    if (selected_media_ != NULL && selected_media_ == &media) {
        VU::ofPush();
        ofSetColor(255, 50, 100);
        ofRect(x, y, w, h);
        VU::ofPop();
    }

    media.draw(x + 5, y + 5, w - 10, h - 10);

    float pos;
    if (media.is_playing()) {
        pos = media.get_position();
    } else {
        pos = 0.0;
    }

    MultFX * m = media.get_mom();
    int i = media.get_mom_index();


    VU::ofPush();
    ofTranslate(x, 10 + y);
    if (m != NULL) {
        drawBoolIndicator(m->transp[i], 20, 20);
        drawBoolIndicator(m->invert[i], 100, 40);
        drawBoolIndicator(m->greysc[i], 200, 60);
    }

    // Progress bar
    ofSetColor(240, 200, 40, 175);
    ofRect(x, y, w * pos, 10);

    // Speed multiplier
    ofSetColor(250);
    ofDrawBitmapString("x" + ofToString(media.get_speed()), x + 0.8 * w, y + 0.8 * h);
    VU::ofPop();
}

void draw_meter(float val, int w, int h)
{
    int bar_w = w / 10;
    VU::ofPush();
    ofSetColor(100);
    ofRect(0, 0, w, h);
    ofSetColor(240, 200, 40);
    ofRect(0 + w*val, 0, bar_w, h);
    VU::ofPop();
}

void MediaRose::draw_media_list(int width, int height)
{
    int row_height = 15;
    list_height_ = height / row_height - 1;
    half_height_ = list_height_ / 2;

    VU::ofPush();
    {
        ofTranslate(10, 0);

        int start = selected_path_ - half_height_;
        if (start > n_paths_ - list_height_) {
            start = n_paths_ - list_height_;
        }
        if (start < 0) {
            start = 0;
        }

        int end = selected_path_ + half_height_;
        if (end < list_height_) {
            end = list_height_;
        }
        if (end > n_paths_) {
            end = n_paths_;
        }

        for (int i = start; i < end; ++i) {
            ofSetColor(140);
            if (i == selected_path_) {
                ofSetColor(250);
            }
            ofDrawBitmapString(video_paths_[i], 0, 0);
            ofTranslate(0, row_height);
        }
    }
    VU::ofPop();

}

void MediaRose::draw_settings(InputData& l, int setting_width, int setting_height)
{
    int w = setting_width / 4;
    int h = setting_height / 4;

    draw_vid_thumb(media0_, 0, 0, w, h);
    draw_vid_thumb(media1_, 1*w, 0, w, h);
    mult0_.draw(2*w, 0, w, h);

    draw_vid_thumb(media2_, 0, h, w, h);
    draw_vid_thumb(media3_, 1*w, h, w, h);
    mult1_.draw(2*w, h, w, h);

    mix_.draw(3*w, h/2, w, h);

    if (n_paths_ > 0) {
        VU::ofPush();
        {
            ofTranslate(0, setting_height / 2 + 10);
            draw_media_list(setting_width, setting_height/2 - 2);
        }
        VU::ofPop();
    }

    VU::ofPush();
    {
        ofTranslate(10, setting_height - 20);
        draw_meter(mix_.fade_value, 30, 10);
        ofTranslate(35, 0);
        draw_meter(mult0_.prominence_0, 30, 10);
        ofTranslate(35, 0);
        draw_meter(mult1_.prominence_0, 30, 10);
        ofTranslate(35, 0);
        draw_meter(mix_.strobe_frequency, 30, 10);
    }
    VU::ofPop();

}

void MediaRose::key_pressed(int key)
{
    // Commands for selecting videos
    if (key == 'q') {
        selected_media_ = & media0_;
    }
    if (key == 'w') {
        selected_media_ = & media1_;
    }
    if (key == 'a') {
        selected_media_ = & media2_;
    }
    if (key == 's') {
        selected_media_ = & media3_;
    }

    // Commands for moving the selection
    if (key == 'k') {
        selected_path_ = (selected_path_ - 1);
    }
    if (key == 'j') {
        selected_path_++;
    }
    if (key == 'g') {
        selected_path_ = 0;
    }
    if (key == 'G') {
        selected_path_ = n_paths_ - 1;
    }
    if (key == 'd') {
        selected_path_ += half_height_;
    }
    if (key == 'u') {
        selected_path_ -= half_height_;
    }

    // Commands for switching media page
    if (key == 'h') {
        selected_page_--;
        page_changed_ = true;
    }
    if (key == 'l') {
        selected_page_++;
        page_changed_ = true;
    }

    if (key == 'c') {
        set_camera(*selected_media_);
    }

    // Correct out of bounds positions
    if (selected_path_ >= n_paths_) {
        selected_path_ = n_paths_ - 1;
    }
    if (selected_path_ < 0) {
        selected_path_ = 0;
    }

    if (selected_page_ >= n_pages_) {
        selected_page_ = n_pages_ - 1;
    }
    if (selected_page_ < 0) {
        selected_page_ = 0;
    }

    if (selected_media_ == NULL) {
        return;
    }

    if (key == 'o') {
        selected_media_->stop();
    }
    if (key == '0') {
        if (selected_media_->is_paused()) {
            selected_media_->unpause();
        } else {
            selected_media_->pause();
        }
    }
    if (key == 'p') {
        selected_media_->play();
    }
    if (key == 'f') {
        set_media(*selected_media_, video_paths_[selected_path_]);
    }
    if (key == 't') {
        // Make a media greyscale
        MultFX * m = selected_media_->get_mom();
        int i = selected_media_->get_mom_index();
        if (m != NULL) {
            bool t = m->greysc[i];
            m->greysc[i] = t ? false : true;
        }
    }
    if (key == 'i') {
        // Invert colors in media's multiplier
        MultFX * m = selected_media_->get_mom();
        int i = selected_media_->get_mom_index();
        if (m != NULL) {
            bool inv = m->invert[i];
            m->invert[i] = inv ? false : true;
        }
    }

    if (key == '1') {
        selected_media_->set_position(0.1);
    }
    if (key == '2') {
        selected_media_->set_position(0.2);
    }
    if (key == '3') {
        selected_media_->set_position(0.3);
    }
    if (key == '4') {
        selected_media_->set_position(0.4);
    }
    if (key == '5') {
        selected_media_->set_position(0.5);
    }
    if (key == '6') {
        selected_media_->set_position(0.6);
    }
    if (key == '7') {
        selected_media_->set_position(0.7);
    }
    if (key == '8') {
        selected_media_->set_position(0.8);
    }
    if (key == '9') {
        selected_media_->set_position(0.9);
    }
}

void MediaRose::reload_shaders() {
    mult0_.reload();
    mult1_.reload();
    mix_.reload();
}
