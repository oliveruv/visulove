// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include <string>

#include "Media.h"
#include "Settings.h"

inline bool ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

Media::Media()
{
    is_paused_ = false;
    is_playing_ = false;
    backend_ = Backend::NoBackend;
    mom_index_= 0;
    mom_ = NULL;
}

Media::~Media()
{
}

void Media::close_source()
{
    if (backend_ == Backend::Video || backend_ == Backend::Gif) {
        video_player_.closeMovie();
    } else if (backend_ == Backend::Camera) {
        cam_.close();
    }

    backend_ = Backend::NoBackend;
}

void Media::set_source(std::string path)
{
    close_source();

    // Check if path ends with gif
    if (ends_with(path, ".gif")) {
        backend_ = Backend::Gif;
    } else {
        backend_ = Backend::Video;
    }

    bool loaded = false;
    bool decoded = false;

    try {
        switch (backend_) {
        case Backend::Video:
            video_player_.loadMovie(path);
            video_player_.setLoopState(OF_LOOP_NORMAL);
            video_player_.setVolume(0.0f);
            video_player_.play();
            loaded = true;
            break;
        case Backend::Gif:
            gif_position_ = 0;
            gif_i_ = 0;
            decoded = gif_decoder_.decode(path);
            if (decoded) {
                gif_file_ = gif_decoder_.getFile();
                height_ = gif_file_.getHeight();
                width_ = gif_file_.getWidth();
                loaded = true;
            }
            break;
        default:
            break;
        }
    } catch (const std::exception& e) {
        ofLog(OF_LOG_ERROR, "Caught exception loading media: %s", path.c_str());
    }

    if (loaded) {
        is_paused_ = false;
        is_playing_ = true;
    } else {
        is_paused_ = false;
        is_playing_ = false;
    }
}

void Media::set_source_camera()
{
    close_source();

    backend_ = Backend::Camera;

    int cam_id = Settings::get_int("camera:id");
    int cam_width = Settings::get_int("camera:width");
    int cam_height = Settings::get_int("camera:height");

    cam_.setDeviceID(cam_id);
    cam_.initGrabber(cam_width, cam_height);
}

bool Media::has_new_frame()
{
    switch (backend_) {
    case Backend::NoBackend:
        return false;
    case Backend::Video:
        return video_player_.isFrameNew();
    case Backend::Gif:
        return (!is_paused_ && is_playing_);
    case Backend::Camera:
        return cam_.isFrameNew();
    default:
        return false;
    }
}

void Media::update()
{
    switch (backend_) {
    case Backend::NoBackend:
        break;
    case Backend::Video:
        video_player_.update();
        break;
    case Backend::Gif:
        if (!is_paused_ && is_playing_) {
            // TODO sync
            gif_i_++;
            if (gif_i_ == 15) {
                gif_position_ = ++gif_position_ % gif_file_.getNumFrames();
                gif_i_ = 0;
            }
        }
        break;
    case Backend::Camera:
        if (!is_paused_ && is_playing_) {
            cam_.update();
        }
        break;
    default:
        break;
    }
}

void Media::play()
{
    is_playing_ = true;

    switch (backend_) {
    case Backend::NoBackend:
        break;
    case Backend::Video:
        video_player_.play();
        break;
    case Backend::Gif:
    case Backend::Camera:
        break;
    default:
        break;
    }
}

void Media::stop()
{
    is_playing_ = false;

    switch (backend_) {
    case Backend::NoBackend:
        break;
    case Backend::Video:
        video_player_.stop();
        break;
    case Backend::Gif:
        break;
    case Backend::Camera:
        break;
    default:
        break;
    }
}

void Media::pause()
{
    is_paused_ = true;

    switch (backend_) {
    case Backend::NoBackend:
        break;
    case Backend::Video:
        video_player_.setPaused(true);
        break;
    case Backend::Gif:
        break;
    case Backend::Camera:
        break;
    default:
        break;
    }
}

void Media::unpause()
{
    is_paused_ = false;

    switch (backend_) {
    case Backend::NoBackend:
        break;
    case Backend::Video:
        video_player_.setPaused(false);
        break;
    case Backend::Gif:
        break;
    case Backend::Camera:
        break;
    default:
        break;
    }
}

void Media::set_position(float pos)
{
    switch (backend_) {
    case Backend::NoBackend:
        break;
    case Backend::Video:
        video_player_.setPosition(pos);
        break;
    case Backend::Gif:
        gif_position_ = pos * gif_file_.getNumFrames();
        break;
    case Backend::Camera:
        break;
    default:
        break;
    }
}

float Media::get_position()
{
    switch (backend_) {
    case Backend::NoBackend:
        return 0;
    case Backend::Video:
        if (is_playing_) {
            return video_player_.getPosition();
        } else {
            return 0;
        }
    case Backend::Gif:
        return gif_position_ / (double) gif_file_.getNumFrames();
    case Backend::Camera:
        return 1;
    default:
        return 0;
    }
}

float Media::get_speed()
{
    switch (backend_) {
    case Backend::NoBackend:
        return 0;
    case Backend::Video:
        return video_player_.getSpeed();
    case Backend::Gif:
        return 1;
    case Backend::Camera:
        return 1;
    default:
        return 0;
    }
}

bool Media::is_playing()
{
    return is_playing_;
}

bool Media::is_paused()
{
    return is_paused_;
}

void Media::set_mom(MultFX * mom, int index)
{
    mom_ = mom;
    mom_index_ = index;
}

MultFX * Media::get_mom()
{
    return mom_;
}

int Media::get_mom_index()
{
    return mom_index_;
}

void Media::draw(float x, float y, float w, float h)
{
    try {
        switch (backend_) {
        case Backend::NoBackend:
            break;
        case Backend::Video:
            video_player_.draw(x, y, w, h);
            break;
        case Backend::Gif:
            gif_file_.drawFrame(gif_position_, x, y, w, h);
            break;
        case Backend::Camera:
            cam_.draw(x, y, w, h);
            break;
        default:
            break;
        }
    } catch (const std::exception& e) {
        ofLog(OF_LOG_ERROR, "Caught exception trying to draw media.");
    }
}

void Media::draw(float x, float y)
{
    try {
        switch (backend_) {
        case Backend::NoBackend:
            break;
        case Backend::Video:
            video_player_.draw(x, y);
            break;
        case Backend::Gif:
            gif_file_.drawFrame(gif_position_, x, y);
            break;
        case Backend::Camera:
            cam_.draw(x, y);
            break;
        default:
            break;
        }
    } catch (const std::exception& e) {
        ofLog(OF_LOG_ERROR, "Caught exception trying to draw media.");
    }
}

float Media::getHeight()
{
    switch (backend_) {
    case Backend::NoBackend:
        return 0;
    case Backend::Video:
        return video_player_.getHeight();
    case Backend::Gif:
        return height_;
    case Backend::Camera:
        return cam_.getHeight();
    default:
        return 0;
    }
}

float Media::getWidth()
{
    switch (backend_) {
    case Backend::NoBackend:
        return 0;
    case Backend::Video:
        return video_player_.getWidth();
    case Backend::Gif:
        return width_;
    case Backend::Camera:
        return cam_.getWidth();
    default:
        return 0;
    }
}
