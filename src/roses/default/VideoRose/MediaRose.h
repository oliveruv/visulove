// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include <vector>

#include "ofMain.h"
#include "ofxXmlSettings.h"

#include "Media.h"
#include "InputData.h"
#include "MixFX.h"
#include "MultFX.h"
#include "Rose.h"

class MediaRose : public Rose {
    public:
        explicit MediaRose();
        ~MediaRose();

        void setup(Rose* first_child, int output_width, int output_height);
        void load_media_list();

        void update(InputData& l);
        void handle_midi(ofxMidiMessage midi);

        void draw(InputData& l);
        void draw_media_list(int width, int height);
        void draw_settings(InputData& l, int setting_width, int setting_height);
        void draw_vid_thumb(Media & media, int x, int y, int w, int h);

        void set_media(Media & media, std::string path);
        void set_camera(Media & media);

        void key_pressed(int key);

        void reload_shaders();

        string author_name() {return "default";}
        string rose_name() {return "MediaRose";}

    private:
        MediaRose(const MediaRose &) = delete;
        MediaRose(MediaRose &&) = delete;
        MediaRose & operator = (const MediaRose &) = delete;
        MediaRose & operator = (MediaRose &&) = delete;

        ofxXmlSettings media_xml_;
        std::vector<std::string> video_paths_;
        int n_pages_;
        int n_paths_;
        int selected_path_;

        int list_height_;
        int half_height_;

        Media media0_;
        Media media1_;
        Media media2_;
        Media media3_;

        int prev_selected_page_;
        int selected_page_;
        Media * selected_media_;
        bool page_changed_;

        MultFX mult0_;
        MultFX mult1_;
        MixFX mix_;
};
