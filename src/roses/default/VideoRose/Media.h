// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include <string>

#include "MultFX.h"
#include "ofMain.h"
#include "ofxGifDecoder.h" // TODO global decoder

namespace Backend {
    enum Backend {
        Video,
        Gif,
        Camera,
        NoBackend
    };
}; // namespace Backend

class Media final : public ofBaseDraws {
private:
    // CLEAN turn this into an enum class when c++11 rolls around

public:
    explicit Media();
    ~Media();

    void set_source(std::string path);
    void set_source_camera();
    void close_source();

    bool has_new_frame();
    void update();

    void play();
    void stop();
    void pause();
    void unpause();
    void set_position(float pos);

    float get_position();
    float get_speed();
    bool is_playing();
    bool is_paused();

    void set_mom(MultFX * mom, int index);
    MultFX * get_mom();
    int get_mom_index();

    // ofBaseDraws
    virtual void draw(float x, float y);
    virtual void draw(float x, float y, float w, float h);
    virtual float getHeight();
    virtual float getWidth();

private:
    Media(const Media &) = delete;
    Media(Media &&) = delete;
    Media & operator = (const Media &) = delete;
    Media & operator = (Media &&) = delete;

    Backend::Backend backend_;
    ofxGifFile gif_file_;
    ofVideoPlayer video_player_;
    ofVideoGrabber cam_;
    ofxGifDecoder gif_decoder_;

    MultFX * mom_;
    int mom_index_;

    int gif_position_;
    int gif_i_;

    // gif heights and widths are sometimes inconsistent between frames
    int height_;
    int width_;

    bool is_paused_;
    bool is_playing_;
};
