// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include "TransitionRose.h"
#include "VisuUtilities.h"
#include "TransitionType.h"

void TransitionRose::setup(Rose* first_child, int output_width, int output_height) {
    this->output_width = output_width;
    this->output_height = output_height;
    this->previous_rose = first_child;

    ofxKeyframeAnimRegisterEvents(this);

    ofTrueTypeFont::setGlobalDpi(96);
    text_font.loadFont(data_ref("fonts/QuattrocentoSans-Bold.ttf"), 10, true);

    ofBackground(0);
    render_buffer.begin();
    ofClear(0,255);
    render_buffer.end();
}

void TransitionRose::update(InputData& l) {
    this->l = &l;

    if (previous_rose)
        previous_rose->update(l);

    playlist.update();
}

void TransitionRose::draw(InputData& l) {
    this->l = &l;

    if (previous_rose)
        previous_rose->draw(l);

    ofSetColor(0, 0, 0, 255);
    ofEnableAlphaBlending();
    ofDisableDepthTest();
    ofRect(0, 0, progress * output_width, output_height);

    string text = "transition rose";
    ofRectangle title_box = text_font.getStringBoundingBox(text, 0, 0);
    ofVec2f title_delta = VU::getOffset(title_box);
    int title_x = output_width/2 + title_delta.x;
    int title_y = output_height/2 + title_delta.y;
    text_font.drawStringAsShapes(text, title_x, title_y);
}

void TransitionRose::onKeyframe(ofxPlaylistEventArgs& args) {

}

void TransitionRose::do_transition(TransitionType t, Rose* next_rose) {

}
