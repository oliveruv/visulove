// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "ofFbo.h"
#include "ofxFXObject.h"
#include "ofxPlaylist.h"

#include "Rose.h"
#include "InputData.h"
#include "TransitionType.h"

class TransitionRose : public Rose {

    public:
        void setup(Rose* first_child, int output_width, int output_height);
        void update(InputData& l);
        void draw(InputData& l);
        void do_transition(TransitionType t, Rose* next_rose);

        string author_name() {return "default";}
        string rose_name() {return "TransitionRose";}

        void onKeyframe(ofxPlaylistEventArgs& args);

    private:
        ofFbo render_buffer;
        ofxPlaylist playlist;
        ofTrueTypeFont text_font;
        float progress;

        Rose* previous_rose;
        InputData* l;
};
