// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.
//
// With some code copied from the ofxMSAFluid project,
// (c) 2008-2012 Memo Akten, licensed under MIT.

#include "FeedbackRose.h"
#include "Settings.h"
#include "VisuUtilities.h"

char sz[] = "[Rd9?-2XaUP0QY[hO%9QTYQ`-W`QZhcccYQY[`b";
float tuioXScaler = 1;
float tuioYScaler = 1;

FeedbackRose::~FeedbackRose() {
    gui.hide();
}

void FeedbackRose::setup(Rose* first_child, int output_width, int output_height) {
    this->output_width = output_width;
    this->output_height = output_height;
    if (first_child)
        delete first_child;

    int l = cam.listDevices().size();
    for (int i = 0; i < l; ++i) {
        ofVideoDevice dev = cam.listDevices()[i];
        ofLog(OF_LOG_VERBOSE, "Camera id %d, HW %s, DEV: %s", dev.id, dev.hardwareName.c_str(), dev.deviceName.c_str());
    }

    int cam_id = Settings::get_int("camera:id");
    int cam_width = Settings::get_int("camera:width");
    int cam_height = Settings::get_int("camera:height");

    ofLog(OF_LOG_VERBOSE, "CAM id: %d, w: %d, h: %d", cam_id, cam_width, cam_height);

    cam.setDeviceID(cam_id);
    cam.initGrabber(cam_width, cam_height);

    {
        using namespace ofxCv;
        using namespace cv;
        imitate(previous_image, cam);
        imitate(image_diff, cam);
        // imitate() will set up previous and diff
        // so they have the same size and type as cam
    }

    {
        for(int i=0; i<strlen(sz); i++) sz[i] += 20;

        // setup fluid stuff
        fluidSolver.setup(100, 100);
        fluidSolver.enableRGB(true).setFadeSpeed(0.002).setDeltaT(0.5).setVisc(0.00015).setColorDiffusion(0);
        fluidDrawer.setup(&fluidSolver);

        fluidCellsX = 150;

        drawFluid = true;
        drawParticles = false;

        particles_added = 0;

        gui.show();
        gui.addSlider("fluidCellsX", fluidCellsX, 20, 400);
        gui.addButton("resizeFluid", resizeFluid);
        gui.addSlider("colorMult", colorMult, 0, 100);
        gui.addSlider("velocityMult", velocityMult, 0, 100);
        gui.addSlider("fs.viscocity", fluidSolver.viscocity, 0.0, 0.01);
        gui.addSlider("fs.colorDiffusion", fluidSolver.colorDiffusion, 0.0, 0.0003); 
        gui.addSlider("fs.fadeSpeed", fluidSolver.fadeSpeed, 0.0, 0.1); 
        gui.addSlider("fs.solverIterations", fluidSolver.solverIterations, 1, 50); 
        gui.addSlider("fs.deltaT", fluidSolver.deltaT, 0.1, 5);
        gui.addComboBox("fd.drawMode", (int&)fluidDrawer.drawMode, msa::fluid::getDrawModeTitles());
        gui.addToggle("fs.doRGB", fluidSolver.doRGB); 
        gui.addToggle("fs.doVorticityConfinement", fluidSolver.doVorticityConfinement); 
        gui.addToggle("drawFluid", drawFluid); 
        gui.addToggle("drawParticles", drawParticles); 
        gui.addToggle("fs.wrapX", fluidSolver.wrap_x);
        gui.addToggle("fs.wrapY", fluidSolver.wrap_y);
        gui.addSlider("tuioXScaler", tuioXScaler, 0, 2);
        gui.addSlider("tuioYScaler", tuioYScaler, 0, 2);

        gui.currentPage().setXMLName(data_ref("FeedbackSettings.xml"));
        gui.loadFromXML();
        gui.setDefaultKeys(true);
        gui.setAutoSave(false);
        gui.show();

        pMouse = msa::getWindowCenter();
        resizeFluid = true;
    }
}

void FeedbackRose::update(InputData& l) {
    cam.update();
    if (cam.isFrameNew()) {
        using namespace ofxCv;
        using namespace cv;

        // put grayscale cam edges in gray
        /* convertColor(cam, image_gray, CV_RGB2GRAY); */
        /* Canny(image_gray, image_edge, 20, 20, 3); // TODO fiddle 2020 params */
        /* image_edge.update(); */
        /* return; */

        // take the absolute difference of prev and cam and save it inside diff
        absdiff(previous_image, cam, image_diff);
        image_diff.update();

        // like ofSetPixels, but more concise and cross-toolkit
        copy(cam, previous_image);

        // mean() returns a Scalar. it's a cv:: function so we have to pass a Mat
        diffMean = mean(toCv(image_diff));

        // you can only do math between Scalars,
        // but it's easy to make a Scalar from an int (shown here)
        diffMean *= Scalar(50);
    }

    // Turn diff into vectors and add to fluid
    // OF_IMAGE_GRAYSCALE, OF_IMAGE_COLOR, OF_IMAGE_COLOR_ALPHA

    int w = image_diff.getWidth();
    int h = image_diff.getHeight();

    int step_size = (w * h) / (360 * 480);

    for (int x = 0; x < w; x += step_size) {
        for (int y = 0; y < h; y += step_size) {
            if (x >= w || y >= h || ofRandom(1.0) <= 0.01) {
                break;
            }

            ofFloatColor c = image_diff.getColor(x, y);
            float intensity = c.getLightness();
            if (intensity > 0.05) {
                ofVec2f pos(1.0 - (static_cast<float>(x) / static_cast<float>(w)),
                            (static_cast<float>(y) / static_cast<float>(h)));
                c.setSaturation(1.0);
                c.setBrightness(1.0);
                addToFluid(pos, ofVec2f(ofRandom(0.8, ofRandom(0.8))), true, true, c);
            }
        }
    }

    // Particle system
    if (resizeFluid) {
        fluidSolver.setSize(fluidCellsX, fluidCellsX / msa::getWindowAspectRatio());
        fluidDrawer.setup(&fluidSolver);
        resizeFluid = false;
    }

    fluidSolver.update();
}

void FeedbackRose::fadeToColor(float r, float g, float b, float speed) {
    glColor4f(r, g, b, speed);
    ofRect(0, 0, ofGetWidth(), ofGetHeight());
}

// add force and dye to fluid, and create particles
void FeedbackRose::addToFluid(ofVec2f pos, ofVec2f vel, bool addColor, bool addForce, ofColor color) {
    float speed = vel.x * vel.x  + vel.y * vel.y * msa::getWindowAspectRatio() * msa::getWindowAspectRatio();    // balance the x and y components of speed with the screen aspect ratio
    if(speed > 0) {
        pos.x = ofClamp(pos.x, 0.0f, 1.0f);
        pos.y = ofClamp(pos.y, 0.0f, 1.0f);

        int index = fluidSolver.getIndexForPos(pos);

        if(addColor) {
            // Color drawColor(CM_HSV, (getElapsedFrames() % 360) / 360.0f, 1, 1);
            /* ofColor drawColor; */
            /* drawColor.setHsb((ofGetFrameNum() % 255), 255, 255); */

            fluidSolver.addColorAtIndex(index, color * colorMult);

            if(drawParticles)
                particleSystem.addParticles(pos * ofVec2f(ofGetWindowSize()), 10);
        }

        if(addForce)
            fluidSolver.addForceAtIndex(index, vel * velocityMult);
    }
}

void FeedbackRose::draw(InputData& l) {

    if(drawFluid) {
        ofClear(0);
        glColor3f(1, 1, 1);
        fluidDrawer.draw(0, 0, output_width, output_height);
        /* fluidDrawer.draw(1, 0, 256, 64); */
        /* fluidDrawer.draw(2, 65, 384, 384); */
        /* fluidDrawer.draw(2, 449, 512, 192); */
    } else {
        fadeToColor(0, 0, 0, 0.01);
    }

    if(drawParticles) {
        particleSystem.updateAndDraw(fluidSolver, ofGetWindowSize(), drawFluid);
    }

    /* gui.draw(); */

    /* image_edge.draw(0, 0, output_width, output_height); */
    /* image_diff.draw(0, 0, output_width, output_height); */
}

void FeedbackRose::reload_shaders() {
}
