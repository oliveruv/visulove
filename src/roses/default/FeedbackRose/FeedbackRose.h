// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "ofFbo.h"
#include "ofxCv.h"

#include "MSAFluid.h"
#include "ParticleSystem.h"
#include "ofxSimpleGuiToo.h"

#include "Rose.h"
#include "InputData.h"

class FeedbackRose : public Rose {

    public:
        ~FeedbackRose();

        void setup(Rose* first_child, int output_width, int output_height);
        void update(InputData& l);
        void draw(InputData& l);

        void reload_shaders();

        string author_name() {return "default";}
        string rose_name() {return "FeedbackRose";}

        // Particleshits
        void fadeToColor(float r, float g, float b, float speed);
        void addToFluid(ofVec2f pos, ofVec2f vel, bool addColor, bool addForce, ofColor color);

        float colorMult;
        float velocityMult;
        int   fluidCellsX;
        bool  resizeFluid;
        bool  drawFluid;
        bool  drawParticles;

        msa::fluid::Solver   fluidSolver;
        msa::fluid::DrawerGl fluidDrawer;

        ParticleSystem particleSystem;

        ofVec2f pMouse;

        int particles_added;

    private:
        ofVideoGrabber cam;
        ofPixels previous_image;
        ofImage image_diff;
        cv::Scalar diffMean;
        ofxSimpleGuiToo gui;

        ofPixels image_gray;
        ofImage image_edge;
        // a scalar is like an ofVec4f but normally used for storing
        // color information

        ofPlanePrimitive plane;
        ofPoint star_position;

        ofFbo render_buffer;
        ofShader star_shader;

        static const int PLANE_DENSITY_BASE = 80;
        int plane_grid_size;
        int plane_width;
        int plane_height;
        int plane_columns;
        int plane_rows;
        float summed_plane_rotation;
};
