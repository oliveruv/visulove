// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include <algorithm>
#include <math.h>

#include "WelcomeRose.h"
#include "VisuUtilities.h"
#include "DynamicTriangle.h"

void WelcomeRose::setup(Rose* first_child, int output_width, int output_height) {
    this->output_width = output_width;
    this->output_height = output_height;
    if (first_child)
        delete first_child;

    ofxKeyframeAnimRegisterEvents(this);

    should_draw_title = 0;

    num_triangles = 0;
    max_tri_distance = output_height * 3;
    max_tri_num = 1000;
    min_tri_num = 0;
    tri_spawn_chance = 0.5;
    title_alpha = 0;
    background_darkness = 38;
    shapes_alpha = 255;

    ofBackground(0);

    cam.setupPerspective(true, 90, 0, 100000);
    cam.setPosition(0, 0, -10);
    cam.lookAt(ofVec3f(0, 0, 0));

    ofTrueTypeFont::setGlobalDpi(96);
    title_font.loadFont(data_ref("fonts/Ostrich_Sans_Inline-regular.otf"), 200, true, false, true, 0.0, 96);
    /* text_font.loadFont(data_ref("fonts/QuattrocentoSans-Bold.ttf"), 10, true); */

    render_buffer.allocate(output_width, output_height, GL_RGBA);
    title_shader.load(data_ref("title"));
    /* ofx_pastel.allocate(output_width, output_height, GL_RGBA); */
    tri_fx.load(data_ref("cloud"));
    tri_fx.allocate(output_width, output_height, GL_RGBA);

    render_buffer.begin();
    ofClear(0,255);
    render_buffer.end();
}

void WelcomeRose::update(InputData& l) {
    this->l = &l;

    playlist.update();

    update_shapes(l);

    float hue = (l.effect_y_norm + l.object_y_norm) / 2;
    float brightness = 1;
    float saturation = ((1 - l.effect_z_norm) + (1 - l.object_z_norm)) / 2;
    saturation *= ofMap(shapes_alpha, 0, 255, 0, 1, true);
    tri_fx.tint = ofFloatColor::fromHsb( hue, saturation, brightness);
}

void WelcomeRose::draw(InputData& l) {
    this->l = &l;

    ofEnableDepthTest();
    ofEnableAlphaBlending();

    render_buffer.begin();
    {
        ofPushStyle();
        {
            ofClear(1, background_darkness, 122, 255);
            cam.begin();
            draw_shapes();
            cam.end();

            ofDisableDepthTest();
            ofEnableAlphaBlending();
            ofSetColor(1, background_darkness, 122, 255 - shapes_alpha);
            ofRect(0, 0, output_width, output_height);
        }
        ofPopStyle();
    }
    render_buffer.end();

    tri_fx << render_buffer;
    tri_fx.draw(0, 0, output_width, output_height);
    /* render_buffer.draw(0, 0, output_width, output_height); */

    ofPushStyle();
    {
        title_shader.begin();
        VU::bind_shader_vars(title_shader, l, output_width, output_height);
        draw_title();
        title_shader.end();
    }
    ofPopStyle();

    ofSetColor(255, 255);
    /* ofx_pastel << render_buffer; */
    /* ofx_pastel.draw(0, 0, output_width, output_height); */
    ofDrawBitmapString(ofToString(num_triangles, 1) + " triangles", 10, 35);
}

void WelcomeRose::draw_title() {
    string text = STRINGIFY(by Children of Roses\n
            Copyright (c) 2014 Oliver Uvman (oliver@uvman.se)\n
            \n
            No warranties provided or liabilities accepted.\n
            All contributions accepted and given under The MIT License only.\n
            Software and source provided under The MIT License.\n
            See the file LICENSE.txt for explicit conditions.\n
            \n
            Press w for next Rose.\n
            Press q for previous.);

    ofRectangle title_box = title_font.getStringBoundingBox("visulove", 0, 0);
    ofVec2f title_delta = VU::getOffset(title_box);
    int title_x = output_width/2 + title_delta.x;
    int title_y = output_height/2 + title_delta.y;
    ofSetColor(255, 255, 255, (int)title_alpha);
    ofEnableAlphaBlending();
    ofDisableDepthTest();
    title_font.drawStringAsShapes("visulove", title_x, title_y);

    /* ofSetColor(255, 80); */
    /* ofRectangle r = title_font.getStringBoundingBox("visulove", 0, 0); */
    /* text_font.drawString(text, title_x, title_y - title_box.y); */
}


void WelcomeRose::update_shapes(InputData& l) {

    for (int i = 0; i < num_triangles; i++) {
        DynamicTriangle* t = triangles[i];
        // Update triangle distances
        t->distance += 2 + 70 * l.hand_spread;
        if (t->distance > max_tri_distance)
            continue;

        // Update other attributes
        float flicker_magnitude = l.object_spread * 20;
        t->color.g += ofRandom(-flicker_magnitude, flicker_magnitude);
        if (t->color.g > 255)
            t->color = 255;
        if (t->color.g < 0)
            t->color = 0;
    }

    prune_triangles();  // Clear outdated triangles

    // Attempt to approach this number of triangles
    int tri_goal_num = (int) ofLerp(min_tri_num, max_tri_num, l.effect_spread);

    if (num_triangles < tri_goal_num) {
        tri_spawn_chance = 1 - ((float)num_triangles) / ((float)tri_goal_num);
        if (ofRandom(1) < tri_spawn_chance) {
            DynamicTriangle* t = new DynamicTriangle();
            t->center = ofPoint(0, 0);
            t->distance = 0;
            t->bredth = 45;
            t->rotation = 0;
            t->color = ofColor((int)ofRandom(255), 125, (int)ofRandom(255), 255);
            triangles.push_back(t);
            num_triangles += 1;
        }
    }
}

void WelcomeRose::prune_triangles() {
    for (int i = 0; i < num_triangles; i++) {

        if (triangles[i]->distance > max_tri_distance) {
            triangles.erase(triangles.begin() + i);
            num_triangles -= 1;
            return prune_triangles();
        }

    }
}

void WelcomeRose::draw_shapes() {
    for (int i = 0; i < num_triangles; i++) {
        triangles[i]->draw(shapes_alpha);
    }
}

void WelcomeRose::reload_shaders() {
    title_shader.unload();
    title_shader.load(data_ref("title"));
    tri_fx.reload();
}

void toggle(bool& b) {
    if (b) {
        b = false;
    }
    else {
        b = true;
    }
}

void WelcomeRose::onKeyframe(ofxPlaylistEventArgs& args) {
    if (args.message == "ADD_TRIANGLE"){
    }
}

void WelcomeRose::alpha() {
    toggle(should_draw_title);
    if (should_draw_title) {
        playlist.clear();
        using namespace Playlist;
        playlist.addKeyFrame(Action::tween(300, &shapes_alpha, 7, TWEEN_LIN));

        playlist.addKeyFrame(Action::tween(400, &shapes_alpha, 0, TWEEN_CIRC, TWEEN_EASE_OUT));
        playlist.addToKeyFrame(Action::tween(400, &background_darkness, 5, TWEEN_QUAD, TWEEN_EASE_OUT));
        playlist.addToKeyFrame(Action::tween(400, &title_alpha, 255, TWEEN_QUAD, TWEEN_EASE_IN_OUT));

        playlist.addKeyFrame(Action::tween(400, &background_darkness, 38, TWEEN_QUAD, TWEEN_EASE_OUT));
    } else {
        playlist.clear();
        using namespace Playlist;
        playlist.addKeyFrame(Action::tween(200, &title_alpha, 0, TWEEN_QUAD, TWEEN_EASE_IN_OUT));
        playlist.addKeyFrame(Action::tween(200, &shapes_alpha, 255, TWEEN_QUAD, TWEEN_EASE_IN_OUT));
    }
}
