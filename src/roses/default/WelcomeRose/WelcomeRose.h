// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "ofFbo.h"
#include "ofxFXObject.h"
#include "ofxBokeh.h"
#include "ofxPlaylist.h"

#include "DynamicTriangle.h"
#include "Rose.h"
#include "TriFX.h"
#include "InputData.h"

class WelcomeRose : public Rose {

    public:
        void setup(Rose* first_child, int output_width, int output_height);
        void update(InputData& l);
        void draw(InputData& l);
        void reload_shaders();

        void alpha();

        string author_name() {return "default";}
        string rose_name() {return "WelcomeRose";}

        void onKeyframe(ofxPlaylistEventArgs& args);

    private:
        ofEasyCam cam;
        ofFbo render_buffer;
        TriFX tri_fx;
        ofShader title_shader;
        ofTrueTypeFont title_font;
        ofTrueTypeFont text_font;
        ofxPlaylist playlist;
        DynamicTriangle tris;

        InputData* l;

        bool should_draw_title;
        float title_alpha;
        float shapes_alpha;
        float background_darkness;

        int num_triangles;
        int max_tri_distance;
        int max_tri_num;
        int min_tri_num;
        float tri_spawn_chance;

        vector<DynamicTriangle*> triangles;

        void update_shapes(InputData& l);
        void prune_triangles();
        void draw_shapes();
        void draw_title();
};
