// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"
#include "ofxFXObject.h"

class TriFX : public ofxFXObject {
    public:
        ofFloatColor tint;

        void injectUniforms() {
            float c[4] = {tint.r, tint.g, tint.b, tint.a};
            shader.setUniform4fv("tint", &c[0]);
        }
};
