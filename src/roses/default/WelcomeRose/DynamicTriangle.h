#pragma once

#include <math.h>

#include "VisuUtilities.h"
#include "ofxPlaylist.h"

class DynamicTriangle {
    public:
        ofPoint center;
        float distance;
        float bredth;
        float rotation;
        ofColor color;

        static void draw(ofPoint center, float distance, float bredth, float rotation, ofColor color) {
            ofPushStyle();

            float height = (distance * distance)/100;

            ofVec3f axis(0, 0, 1);
            float b = tan(VU::deg_to_rad(bredth)) * height;
            float r = height/2;

            ofPoint top(0);
            top.y -= r;

            ofPoint right(0);
            right.x += b;
            right.y += r;

            ofPoint left(0);
            left.x -= b;
            left.y += r;

            top.rotate(rotation, axis);
            right.rotate(rotation, axis);
            left.rotate(rotation, axis);

            center.z = distance * 2;
            top += center;
            left += center;
            right += center;

            ofSetColor(color);
            ofTriangle(top, right, left);
            // TODO things refuse to smooth/antialias.
            /* ofEnableAntiAliasing(); */
            /* ofEnableSmoothing(); */
            /* ofNoFill(); */
            /* ofTriangle(top, right, left); */
            /* ofLine(top, right); */
            /* ofLine(right, left); */
            /* ofLine(left, top); */
            ofPopStyle();
        }

        void draw(float alpha_mod) {
            ofColor c = ofColor(color);
            c.a = alpha_mod;
            DynamicTriangle::draw(center, distance, bredth, rotation, color);
        }
};
