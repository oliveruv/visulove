// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "VisuPlaylist.h"
#include "RoseCreator.h"
#include "TransitionType.h"

#include <vector>

class LinearPlaylist : VisuPlaylist {
    private:
        struct Track {
            RoseCreator creator;           // A function creating the Rose to switch into.
            TransitionType out_transition; // Which transition method to use when switching next Rose.
        };

        vector<Track> tracks;
        int position;

        int next_id() {
            return (position + 1) % (tracks.size() - 1);
        }

        int previous_id() {
            int prev_id = (position - 1);
            if (prev_id < 0)
                prev_id = tracks.size() - 1;
            return prev_id;
        }

    public:
        RoseCreator start() {
            position = 0;
            return tracks[position].creator;
        }

        RoseCreator next_rose() {
            position = next_id();
            return tracks[position].creator;
        }

        RoseCreator previous_rose() {
            position = previous_id();
            return tracks[position].creator;
        }

        TransitionType peek_transition() {
            return tracks[position].out_transition;
        }

        void add_rose(RoseCreator rose_creator, TransitionType out_transition) {
            Track t = {rose_creator, out_transition};
            tracks.push_back(t);
        }
};
