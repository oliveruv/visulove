// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include "InputHandler.h"
#include "Settings.h"

InputHandler::~InputHandler()
{
    if (using_midi_) {
        midi.closePort();
        midi.removeListener(this);
    }
}

void InputHandler::setup(int output_width, int output_height) {
    this->output_width = output_width;
    this->output_height = output_height;
    leap.open();

    using_midi_ = Settings::get_bool("midi:enabled", false);

    if (using_midi_) {
        midi.openPort(1);
        //midiIn.openPort("IAC Pure Data In"); // by name
        //midiIn.openVirtualPort("ofxMidiIn Input"); // open a virtual port

        // don't ignore sysex, timing, & active sense messages,
        // these are ignored by default
        midi.ignoreTypes(false, false, false);

        // add testApp as a listener
        midi.addListener(this);
    }
}

void InputHandler::key_pressed(int key) {
    // TODO let Roses know when keys are held
}

void InputHandler::update() {
    if (next_action == 'w')
        playlist_manager.next();
    if (next_action == 'q')
        playlist_manager.previous();

    if (next_action == 'a')
        playlist_manager.alpha();
    if (next_action == 's')
        playlist_manager.beta();
    if (next_action == 'd')
        playlist_manager.gamma();

    if (next_action == 'm') {
        playlist_manager.reload_shaders();
        reconnect_midi();
    }

    if (next_action != ' ') {
        playlist_manager.key_pressed(next_action);
    }

    next_action = ' ';
}

void InputHandler::key_released(int key) {
    next_action = key;
}

float InputHandler::get_simple_finger_spread(ofxLeapMotionSimpleHand hand) {
    // Assumes we have mapped leap to our world space...
    unsigned int i;
    unsigned int j;

    if (hand.fingers.size() < 2)
        return 0.2;

    float widest_spread = NARROWEST_FINGER_SPREAD_POSSIBLE;
    for (i = 0; i < hand.fingers.size(); ++i)
    {
        ofPoint iPos = hand.fingers[i].pos;

        for (j = 0; j < hand.fingers.size(); ++j)
        {
            if (i == j)
                continue;
            float spread = iPos.distance(hand.fingers[j].pos);
            if (spread > widest_spread)
                widest_spread = spread;
        }
    }
    return ofMap(widest_spread, NARROWEST_FINGER_SPREAD_POSSIBLE, WIDEST_FINGER_SPREAD_POSSIBLE, 0, 1, true);
}

ofxLeapMotionSimpleHand InputHandler::get_rightmost_hand(vector <ofxLeapMotionSimpleHand> hands) {
    int highest_x_val = -99999;
    ofxLeapMotionSimpleHand rightmost_hand;

    for (unsigned int i = 0; i < hands.size(); i++) {
        if (hands[i].handPos.x > highest_x_val) {
            rightmost_hand = hands[i];
            highest_x_val = rightmost_hand.handPos.x;
        }
    }

    return rightmost_hand;
}

ofxLeapMotionSimpleHand InputHandler::get_leftmost_hand(vector <ofxLeapMotionSimpleHand> hands) {
    int lowest_x_val = 99999;
    ofxLeapMotionSimpleHand leftmost_hand;

    for (unsigned int i = 0; i < hands.size(); i++) {
        if (hands[i].handPos.x < lowest_x_val) {
            leftmost_hand = hands[i];
            lowest_x_val = leftmost_hand.handPos.x;
        }
    }

    return leftmost_hand;
}

void InputHandler::fill_leap_data(InputData& l) {
    simple_hands = leap.getSimpleHands();

    if( leap.isFrameNew() && simple_hands.size() > 1) {

        // Leap returns data in mm - lets set a mapping to our world space.
        leap.setMappingX(-230, 230, -output_width/2, output_width/2);
        leap.setMappingY(90, 490, -output_height/2, output_height/2);
        leap.setMappingZ(-150, 150, -200, 200);

        ofxLeapMotionSimpleHand left_hand = get_leftmost_hand(simple_hands);
        ofxLeapMotionSimpleHand right_hand = get_rightmost_hand(simple_hands);

        ofxLeapMotionSimpleHand effect_hand = right_hand;
        ofxLeapMotionSimpleHand object_hand = left_hand;

        if (INVERT_HANDS) {
            effect_hand = left_hand;
            object_hand = right_hand;
        }

        // TODO Seems like leap's y-axis is inverted. Figure out why,
        // and check whether this is always the case etc.
        l.object_point = object_hand.handPos;
        l.object_point.y *= -1;
        l.object_spread = get_simple_finger_spread(object_hand);

        l.effect_point = effect_hand.handPos;
        l.effect_point.y *= -1;
        l.effect_spread = get_simple_finger_spread(effect_hand);

        float hand_spread = l.object_point.distance(l.effect_point);
        l.hand_spread = ofMap(hand_spread, NARROWEST_HAND_SPREAD_POSSIBLE, WIDEST_HAND_SPREAD_POSSIBLE, 0, 1, true);
    }

    leap.markFrameAsOld();

    // Calculate normal values for input vectors.
    l.object_x_norm = ofNormalize(l.object_point.x, -output_width/2, output_width/2);
    l.object_y_norm = ofNormalize(l.object_point.y, -output_height/2, output_height/2);
    l.object_z_norm = ofNormalize(l.object_point.z, -200, 200);
    l.effect_y_norm = ofNormalize(l.effect_point.y, -output_height/2, output_height/2);
    l.effect_x_norm = ofNormalize(l.effect_point.x, -output_width/2, output_width/2);
    l.effect_z_norm = ofNormalize(l.effect_point.z, -200, 200);

    /* ofLog(OF_LOG_VERBOSE, "object_spread: %f,  effect_spread: %f,  hand_spread: %f", */
    /*         l.object_spread, l.effect_spread, l.hand_spread); */

    /* ofLog(OF_LOG_VERBOSE, "object: x %f,  y %f,  z %f", */
    /*         l.object_point.x, l.object_point.y, l.object_point.z); */
    /* ofLog(OF_LOG_VERBOSE, "x_norm: %f,  y_norm: %f,  z_norm:  %f\n", */
    /*         l.object_x_norm, l.object_y_norm, l.object_z_norm); */

    /* ofLog(OF_LOG_VERBOSE, "effect: Pos: x %f,  y %f,  z %f", */
    /*         l.effect_point.x, l.effect_point.y, l.effect_point.z); */
    /* ofLog(OF_LOG_VERBOSE, "x_norm: %f,  y_norm: %f,  z_norm:  %f\n", */
    /*         l.effect_x_norm, l.effect_y_norm, l.effect_z_norm); */
}


void InputHandler::newMidiMessage(ofxMidiMessage& msg)
{
    // make a copy of the latest message
    if (using_midi_) {
        midi_messages_.push_back(msg);
    }
}

void InputHandler::fill_midi_data(InputData & l)
{
    if (using_midi_) {
        l.midi_messages = midi_messages_; // deep copy
        midi_messages_.clear();
    }
}

void InputHandler::reconnect_midi()
{
    if (using_midi_) {
        midi.closePort();
        midi.removeListener(this);

        midi.openPort(1);
        //midiIn.openPort("IAC Pure Data In"); // by name
        //midiIn.openVirtualPort("ofxMidiIn Input"); // open a virtual port

        // don't ignore sysex, timing, & active sense messages,
        // these are ignored by default
        midi.ignoreTypes(false, false, false);

        // add testApp as a listener
        midi.addListener(this);
    }
}
