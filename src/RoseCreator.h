// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "Rose.h"

// Sort of using this pattern:
// https://stackoverflow.com/questions/1096700/instantiate-class-from-name

template<typename T>
Rose* create_rose() {
    return new T();
}

// A RoseCreator is a pointer to a function that takes no arguments and
// returns a pointer to a Rose.
typedef Rose* (*RoseCreator)();
