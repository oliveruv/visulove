// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#define STRINGIFY(A) #A

// Will make Rose::data_ref reference data in src/roses/author/rosename/
// instead of visulove/data/bin/roses/author/rosename/
#define DEBUG true

#include "InputData.h"
#include "ofMain.h"

class VisuUtilities {
    public:
        static float smootherstep(float x, float edge0, float edge1)
        {
            x = ofClamp((x - edge0)/(edge1 - edge0), 0.0, 1.0);
            return x*x*x*(x*(x*6 - 15) + 10);
        }

        // Maps a [0,1] value to a smootherstep'd [0,1] value
        static float smooth_map(float x) {
            return smootherstep(x, 0.0, 1.0);
        }

        // Returns x and y offsets that will help you center a string around a point.
        // To draw centered on point A, do something like
        // string text = "text to center"
        // ofVec2f delta = getOffset(font.getStringBoundingBox(text));
        // font.drawString(text, x + delta.x, y + delta.y);
        static ofVec2f getOffset(ofRectangle bounding_box){
            ofRectangle r = bounding_box;
            return ofVec2f(floor(-r.x - r.width * 0.5f), floor(-r.y - r.height * 0.5f));
        }

        static float deg_to_rad(float degrees) {
            return 0.0174532925 * degrees;
        }

        static void bind_shader_vars(ofShader shader, InputData& l, int buffer_width, int buffer_height) {
            shader.setUniform1f("time", ofGetElapsedTimef());
            shader.setUniform2f("size", (float)buffer_width, (float)buffer_height);
            shader.setUniform2f("resolution", (float)buffer_width, (float)buffer_height);
            shader.setUniform2f("mouse", (float)(ofGetMouseX()/buffer_width), (float)(ofGetMouseY()/buffer_height));
        }

	static void ofPush()
	{
	    ofPushStyle();
	    ofPushMatrix();
	    ofPushView();
	}

	static void ofPop()
	{
	    ofPopStyle();
	    ofPopMatrix();
	    ofPopView();
	}

	static float normalize_midi(int midi_value)
	{
	    return ofMap(midi_value, 0, 127, 0, 1, true);
	}
};

typedef VisuUtilities VU;
