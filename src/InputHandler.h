// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include <vector>

#include "ofxLeapMotion.h"
#include "ofxMidi.h"
#include "ofMain.h"
#include "InputData.h"
#include "PlaylistManager.h"

class InputHandler : public ofxMidiListener {
    public:
        InputHandler(PlaylistManager& playlist_manager)
            : playlist_manager(playlist_manager) {}

        ~InputHandler();

        void setup(int output_width, int output_height);
        void update();
        void fill_leap_data(InputData& l);
        void fill_midi_data(InputData& l);
        void newMidiMessage(ofxMidiMessage& eventArgs);

        void key_pressed(int key);
        void key_released(int key);

    private:
        // The widest spread I have managed is ~650, the narrowest ~50.
        // TODO this should depend on millimetres, not world positions,
        // and the in-min/in-max should be adjustable per user. Hand
        // spread should be similarly adjustable.
        const static float NARROWEST_FINGER_SPREAD_POSSIBLE = 40;
        const static float WIDEST_FINGER_SPREAD_POSSIBLE = 650;

        const static float NARROWEST_HAND_SPREAD_POSSIBLE = 219;
        const static float WIDEST_HAND_SPREAD_POSSIBLE = 3134;

        const static bool INVERT_HANDS = false;

        void reconnect_midi();

        PlaylistManager& playlist_manager;
        int output_width;
        int output_height;
        int next_action = ' ';

        float get_simple_finger_spread(ofxLeapMotionSimpleHand hand);
        ofxLeapMotionSimpleHand get_rightmost_hand(
                vector <ofxLeapMotionSimpleHand> hands);
        ofxLeapMotionSimpleHand get_leftmost_hand(
                vector <ofxLeapMotionSimpleHand> hands);

        ofxLeapMotion leap;

        bool using_midi_;
        ofxMidiIn midi;
        std::vector<ofxMidiMessage>  midi_messages_;

        vector <ofxLeapMotionSimpleHand> simple_hands;
};
