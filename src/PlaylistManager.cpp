// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include "PlaylistManager.h"
#include "RoseCreator.h"

// For now include Roses here, TODO load them dynamically.
#include "StarRose.h"
#include "WelcomeRose.h"
#include "FeedbackRose.h"
#include "MediaRose.h"

void PlaylistManager::setup(int output_width, int output_height) {
    this->output_width = output_width;
    this->output_height = output_height;

    playlist.add_rose(&create_rose<MediaRose>, WRAPPED);
    // StarRose wants to be WRAPPED by the next Rose
    /* playlist.add_rose(&create_rose<FeedbackRose>, WRAPPED); */
    // StarRose wants to be WRAPPED by the next Rose
    /* playlist.add_rose(&create_rose<StarRose>, WRAPPED); */
    // WelcomeRose wants to WRAP the next Rose
    /* playlist.add_rose(&create_rose<WelcomeRose>, WRAPPING); */

    RoseCreator c = playlist.start();
    top_rose = c();
    top_rose->setup(NULL, output_width, output_height);
}

Rose* PlaylistManager::get_previous_rose() {
    /* RoseCreator c = playlist.next_rose(); */
    /* Rose* s = c(); */
    /* return s; */
}

Rose* PlaylistManager::get_next_rose() {
    /* RoseCreator c = playlist.previous_rose(); */
    /* Rose* s = c(); */
    /* return s; */
}

void PlaylistManager::update(InputData& l) {
    if (top_rose->wants_handover()) {
        Rose* old_top = top_rose;
        top_rose = top_rose->rose_to_promote();
        delete old_top;
    }

    top_rose->update(l);
}

void PlaylistManager::draw_settings(InputData& l, int setting_width, int setting_height) {
    top_rose->draw_settings(l, setting_width, setting_height);
}

void PlaylistManager::draw(InputData& l) {
    top_rose->draw(l);
}

void PlaylistManager::execute_transition(TransitionType t, Rose* new_rose) {
    switch (t) {
        case WRAPPED:
            new_rose->setup(top_rose, output_width, output_height);
            top_rose = new_rose;
            break;
        case WRAPPING:
            new_rose->setup(NULL, output_width, output_height);
            top_rose->do_transition(WRAPPING, new_rose);
            break;
    }
}

void PlaylistManager::next() {
    /* TransitionType t = playlist.peek_transition(); */
    /* Rose* new_rose = get_next_rose(); */
    /* execute_transition(t, new_rose); */
}

void PlaylistManager::previous() {
    /* TransitionType t = playlist.peek_transition(); */
    /* Rose* new_rose = get_previous_rose(); */
    /* execute_transition(t, new_rose); */
}

void PlaylistManager::alpha() {
    top_rose->alpha();
}

void PlaylistManager::beta() {
    top_rose->beta();
}

void PlaylistManager::gamma() {
    top_rose->gamma();
}

void PlaylistManager::key_pressed(int key)
{
    top_rose->key_pressed(key);
}

void PlaylistManager::reload_shaders() {
    top_rose->reload_shaders();
}
