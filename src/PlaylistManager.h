// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "LinearPlaylist.h"

#include "InputData.h"
#include "Rose.h"

class PlaylistManager {
    public:
        void setup(int output_width, int output_height);
        void update(InputData& l);
        void draw(InputData& l);
        void draw_settings(InputData& l, int setting_width, int setting_height);

        void next();
        void previous();

        void alpha();
        void beta();
        void gamma();

        void key_pressed(int key);

        void reload_shaders();

    private:
        int output_width;
        int output_height;

        void execute_transition(TransitionType t, Rose* new_rose);
        Rose* get_next_rose();
        Rose* get_previous_rose();

        Rose* top_rose;
        LinearPlaylist playlist;
};
