// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include "Rose.h"
#include "TransitionType.h"

// By default, no handover animation - just handover to next Rose immediately.
void Rose::do_transition(TransitionType t, Rose* next_rose) {
    wants_handover_ = true;
    handover_to = next_rose;
}

bool Rose::wants_handover() {
    return wants_handover_;
}

Rose* Rose::rose_to_promote() {
    return handover_to;
}
