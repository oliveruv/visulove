from pprint import pprint

def bright(v, power):
    a = v + 1              # 0,1 -> 1,2
    a = a ** power         # 1,2 -> 1,N
    N = 2 ** power
    a = a - 1              # 1,4 -> 0,N-1
    low_bright = a / (N-1) # 0,N-1 -> 0,1 # now darker
    diff = v - low_bright
    high_bright = v + diff
    return high_bright

def bright_diff(power):
    numbers = range(101)
    numbers = map(lambda x: x / 100.0, numbers)
    res = map(lambda v: bright(v, power), numbers)
    diff = map(lambda x, y: x - y, res, numbers)
    return diff

results = map(bright_diff, range(1, 1024))

i = 1
for val_list in results:
    ok = True
    for diff in val_list:
        if diff < 0:
            ok = False
    print str(i) + ' ' + str(ok) + ' sum: ' + str(sum(val_list))
