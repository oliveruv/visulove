// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include "ofApp.h"

void ofApp::setup()
{
    // This is the approximate number of updates we get from the leap per second.
    ofSetFrameRate(60);
    ofEnableAlphaBlending();
    ofSetBackgroundAuto(false);
    ofSetVerticalSync(true);

    ofSetLogLevel(OF_LOG_VERBOSE);
    show_fps_ = Settings::get_bool("debug:showfps");

    width_ = Settings::get_int("application:width");
    height_ = Settings::get_int("application:height");

    gui_width_ = Settings::get_int("gui:width");
    gui_height_ = Settings::get_int("gui:height");

    out_width_ = Settings::get_int("output:width");
    out_height_ = Settings::get_int("output:height");

    out_x_ = Settings::get_int("output:x");
    out_y_ = Settings::get_int("output:y");

    gui_x_ = Settings::get_int("gui:x");
    gui_y_ = Settings::get_int("gui:y");

    playlist_manager_.setup(out_width_, out_height_);
    input_handler_ = new InputHandler(playlist_manager_);
    input_handler_->setup(out_width_, out_height_);
    /* windowResized(width_, height_); */
}

void ofApp::update()
{
    input_handler_->fill_leap_data(l);
    input_handler_->fill_midi_data(l);
    input_handler_->update();
    playlist_manager_.update(l);
}

//--------------------------------------------------------------
void ofApp::draw()
{
    VU::ofPush();
    {
        ofSetColor(0);
        ofRect(
                0,
                0,
                width_,
                height_);
    }
    VU::ofPop();

    VU::ofPush();
    {
        ofTranslate(gui_x_, gui_y_, 0);
        playlist_manager_.draw_settings(l, gui_width_, gui_height_);
    }
    VU::ofPop();

    VU::ofPush();
    {
        ofTranslate(out_x_, out_y_, 0);
        playlist_manager_.draw(l);
    }
    VU::ofPop();

    if (show_fps_) {
        VU::ofPush();
        ofSetColor(255);
        ofDrawBitmapString(ofToString(ofGetFrameRate(), 1) + " fps", 10, 15);
        VU::ofPop();
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    input_handler_->key_pressed(key);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{
    input_handler_->key_released(key);
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y)
{
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{
    /* ofLog(OF_LOG_VERBOSE, "GOT RESIZE w: %d, h: %d", w, h); */
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg)
{
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{
}
