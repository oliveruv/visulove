// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#pragma once

#include "ofMain.h"

#include "InputHandler.h"
#include "InputData.h"
#include "PlaylistManager.h"
#include "Settings.h"

class ofApp : public ofBaseApp {
    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

    private:
        int width_;
        int height_;

        int out_width_;
        int out_height_;

        int gui_width_;
        int gui_height_;

        int out_x_;
        int out_y_;

        int gui_x_;
        int gui_y_;

        PlaylistManager playlist_manager_;
        InputHandler* input_handler_;
        InputData l;
        bool show_fps_;
};
