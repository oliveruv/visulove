// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#include "ofMain.h"
#include "ofApp.h"
#include "ofAppGlutWindow.h"
#include "Settings.h"

#include "ofGLProgrammableRenderer.h"

int main() {

    ofSetCurrentRenderer(ofGLProgrammableRenderer::TYPE);

    int width = Settings::get_int("application:width");
    int height = Settings::get_int("application:height");

    ofAppGlutWindow window;
    ofSetupOpenGL(&window, width, height, OF_WINDOW);
    window.setWindowTitle("visulove");
    ofRunApp(new ofApp());

}
