// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#version 150

uniform float time;
uniform vec2 size;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;

uniform vec2 size0;
uniform vec2 size1;

uniform float kaleido_time;
uniform float kaleido_sides;
uniform float kaleido_opacity;

uniform float fade_value;
uniform float saturation;
uniform float contrast;
uniform float brightness;
uniform float opacity;
uniform float red;
uniform float blue;
uniform float green;

uniform float strobe_red;
uniform float strobe_blue;
uniform float strobe_green;
uniform float strobe_opacity;

out vec4 outputColor;

vec3 increase_brightness(vec3 value) {
    // Brightness - we bring each value closer to white. We cannot bring
    // each value twice as close as we did for contrast, because this
    // turns all blacks grey. The further a value is from white, the
    // less we want to change it. Watch this:

    // at n = 185, we get really big overflows
    // at n = 6 we get some subtle overflows when maxing both
    // brightness and contrast :D
    float n = 6;
    vec3 powr = vec3(n);

    // [0,1] -> [1,2]
    vec3 x = value + 1;
    // [1,2] -> [1,2^n] - here values become shifted towards black
    x = pow(x, powr);
    // [1,2^n] -> [0,2^n-1] - values that were black are now black again
    x = x - 1;
    // [0,2^n-1] -> [0,1]
    vec3 low_brightness = x / (pow(2, n) - 1);
    vec3 brightness_diff = value - low_brightness;
    vec3 high_brightness = value + brightness_diff;

    return high_brightness;
}

vec2 shift(vec2 orig, float mag)
{
    return (sin(orig + 0.1 * mag) / 2) + 0.5;
}

float circlemod(float a, float b)
{
    return (0.5 + sin(a) / 2) * b;
}

float sinemod(float a, float b)
{
    // MOD
    // a :   [0,inf[
    // b :   ]inf,inf[
    // res : [0,b]

    // SINEMOD, same but continous and palindromic

    a = (a / b) * 2 * 3.1416; // a: 0 -> 0, b -> 2pi, >b -> periodic by b?
    a = 0.5 + sin(a) / 2;     // a : [0,1]
    return a * b;             // res : [0,b]
}

void main(){
    vec3 res = vec3(0.0);
    vec3 one = vec3(1.0);
    vec3 two = vec3(2.0);
    float tau = 2.0 * 3.1416;

    vec2 st = gl_FragCoord.st / size;

    // Kaleidoscope
    float texture_shift = kaleido_time * 10.0;
    float sides = floor(kaleido_sides); // copout !
                                            
    vec2 p = st - 0.5;              // start out centered
    float r = length(p);            // radial coord
    float a = atan(p.y, p.x);       // azimuth
    /* a += texture_shift;          // shift along the azimuth (rotate cartesian plane), makes the line visible */
    /* a = mod(a, tau / sides);     // mod so we get some number of sides */
    a = sinemod(a, tau / sides);
    a = circlemod(a, tau / sides);
    a = abs(a - tau / sides / 2.0); // seems to smoothen within each side
    p = r * vec2(cos(a), sin(a));   // back to cartesian
    p += 0.5;                       // shift back from centered

    // shift kaleidoscope'd textures so we get palindromic wrapping textures
    // and optionally inward/outward movement of textures (set mag to texture_shift)
    p = shift(p, 0);

    vec3 kaleido_frag_0 = texture(tex0, p * size0).rgb;
    vec3 kaleido_frag_1 = texture(tex1, p * size1).rgb;

    vec3 frag_0 = texture(tex0, st * size0).rgb;
    vec3 frag_1 = texture(tex1, st * size1).rgb;

    frag_0 = mix(frag_0, kaleido_frag_0, kaleido_opacity);
    frag_1 = mix(frag_1, kaleido_frag_1, kaleido_opacity);

    // Fade
    res += frag_0 * (1.0 - fade_value);
    res += frag_1 * fade_value;

    // Overlay
    /* vec3 top = two * frag_0 * frag_1; */
    /* vec3 bot = one - two * (one - frag_0) * (one - frag_1); */
    /* /1* res = mix(top, bot, frag_0); *1/ */

    /* vec3 mult = frag_0 * frag_1; */
    /* vec3 sum =  frag_0 * (1.0 - fade_value); */
    /*      sum += frag_1 * fade_value; */
    /* res = mix(sum, mult, frag_0); */

    // Calculate luminance before applying any effects so that values
    // are accurate.
    const vec3 luminance_coefficients = vec3(0.2125, 0.7154, 0.0721);
    vec3 luminance = vec3(dot(res, luminance_coefficients));

    // Contrast - we create this by pushing each color value further
    // from mid-contrast, each color value is pushed twice as far away
    // from 0.5 as it already is.
    vec3 mid = vec3(0.5);
    vec3 contrast_diff = res - mid;
    vec3 high_contrast = res + contrast_diff / 2;

    // Apply effects
    vec3 out_color = res;
    out_color = mix(out_color, high_contrast, contrast);

    // We apply brightness to an already applied contrast, or
    // it'll drown out the contrast effect entirely:
    out_color = mix(out_color, increase_brightness(out_color), brightness);
    out_color *= vec3(red, green, blue);
    out_color = mix(luminance, out_color, saturation);

    // Now we apply strobe, if needed
    vec3 strobe = vec3(strobe_red, strobe_green, strobe_blue);

    // We use this technique to support black strobe
    out_color = mix(out_color, strobe, strobe_opacity);

    // And this to avoid strobing looking bland as it does if only
    // the technique above is used.
    out_color += strobe * strobe_opacity;

    // Last of all we reduce the opacity if needed
    out_color *= opacity;
    outputColor.rgb = out_color;
    outputColor.a = 1.0;
}
