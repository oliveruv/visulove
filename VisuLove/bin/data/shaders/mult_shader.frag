// Copyright (c) 2014
// Oliver Uvman (oliver@uvman.se)
// No warranties provided or liabilities accepted.
// All contributions accepted and given under The MIT License only.
// Software and source provided under The MIT License.
// See the file LICENSE.txt for explicit conditions.

#version 150

uniform float time;
uniform vec2 size;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;

uniform vec2 size0;
uniform vec2 size1;

uniform float prominence_0;
uniform float prominence_1;

uniform float invert_0;
uniform float invert_1;

uniform float transp_0;
uniform float transp_1;

uniform float greysc_0;
uniform float greysc_1;

out vec4 outputColor;

float luminance(vec3 frag) {
    const vec3 luminance_coefficients = vec3(0.2125, 0.7154, 0.0721);
    return dot(frag, luminance_coefficients);
}

vec3 vuminance(vec3 frag) {
    return vec3(luminance(frag));
}

void main(){
    vec2 st = gl_FragCoord.st;

    vec3 white = vec3(1.0);
    vec3 black = vec3(0.0);

    vec3 frag_0 = texture(tex0, st * (size0 / size)).rgb;
    vec3 frag_1 = texture(tex1, st * (size1 / size)).rgb;

    // transparency of whites
    float tr_0_factor = luminance(frag_0);
    tr_0_factor *= transp_0;
    if (luminance(frag_0) >= 0.90)
	frag_0 = mix(frag_0, black, tr_0_factor);

    float tr_1_factor = luminance(frag_1);
    tr_1_factor *= transp_1;
    if (luminance(frag_1) >= 0.90)
	frag_1 = mix(frag_1, black, tr_1_factor);

    // greyscaling of things
    frag_0 = mix(frag_0, vuminance(frag_0), greysc_0);
    frag_1 = mix(frag_1, vuminance(frag_1), greysc_1);

    // inversion of colors
    vec3 inv_0 = white - frag_0;
    vec3 inv_1 = white - frag_1;

    frag_0 = mix(frag_0, inv_0, invert_0);
    frag_1 = mix(frag_1, inv_1, invert_1);

    vec3 frag_0_mod = mix(white, frag_0, prominence_0);
    vec3 frag_1_mod = mix(white, frag_1, prominence_1);

    outputColor.rgb = frag_0_mod * frag_1_mod;
    outputColor.a = 1.0;
}
