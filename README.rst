========
VISULOVE
========

Warning
=======

This project is not maintained. It was built for openFrameworks 0.7.X.
As far as I know, it doesn't compile for newer versions. Getting it
to work would probably be a 3-8 hour endeavour. It is uploaded to the
public mainly as a record of what I did for my bachelor's project
at Linköping University's Computer Science program. The state of
the program at the completion of the thesis is available on branch
``thesis``. See the file ``thesis_report.pdf`` for more information on
that.

I do not provide my linux supporting fork of ofxLeapMotion
as it contained the LeapSDK dynamic library, redistribution
of which is not allowed. Your best bet is to use `this addon
<https://github.com/genekogan/ofxLeapMotion2>`_, which claims
to support Linux.

After finishing my thesis, I spent a few frantic nights turning the
codebase into a VJing tool to be used with MIDI controllers. That is
where the ``master`` branch is at. It should be considered Proof of
Concept quality code. Has a bunch of bugs, and terrible architecture,
but was fun to use. :)

Introduction
============

This is a visual arts framework, used to string together units of visual
art called Roses. A Rose can contain animations, effects, and anything
you want, really. Roses can merge into each other, and will hopefully
grace your eyes with many floral patterns in the future to come. :)

The aim of this platform is to become the first wide spread visual
instrument, that can do for the visual part of live performances what
current technology is doing so well for the audio part. We want to
strengthen playfulness, improvisation, and instant action-reaction
co-operation between VJ and musician.


Legal
=====

Copyright (c) 2014 Oliver Uvman (oliver@uvman.se)

No warranties provided or liabilities accepted.
All contributions accepted and given under The MIT License only.
Software and source provided under The MIT License.
See the file LICENSE.txt for explicit conditions.


Architecture
============

The framework is still relatively simple. Below you can read about
the important parts of it. You can see how they tie together in
``VisuLove/src/ofApp.cpp``.

We use constructors for dependency injection, and setup() functions for
setup. We prefer references over pointers, new over malloc, delete over
free. Composition over inheritance, fun over profit.

Rose.h
------

Provides the basic things necessary to produce a Rose usable by the
system. Like most openFrameworks things, the main three functions it
needs to override are update, draw, and setup.

When a Rose is started, it is given the Rose that was playing before
it, and can continue using that to form part of its output. Magic! A
Rose that doesn't want to use its predecessor *must* delete it! In its
destructor, a Rose *must* delete any other Roses it was has been given.
It should also free any other resources it has been using, such as ofFbo
objects, textures or whatever.

A Rose can also request the next Rose to be played from the
PlaylistManager, in order to use it to provide a beautiful transition.
When it is ready to let the next Rose take over, it should call *its
own* do_transition function.

InputData.h
-----------

Each update(), new information from the LeapMotion device is handed to
any active Roses. This is what that data looks like.

InputHandler.h
--------------

This class is responsible for collecting all the data a Rose uses to do
whatever it does. As long as a Rose doesn't use data other than this,
we'll be able to reconstruct its behaviour in the future.

PlaylistManager.h
-----------------

I was tempted to call this the Gardener. :) This class keeps track of
which Rose is currently being played, which have been played and which
are about to be played. It is responsible for initiating transitions
between Roses, while the transitions themselves are parts of each Rose.
 

Installation
============

Note that all paths below are case sensitive. The makefile system needs
correctly named folders in ``addons/`` to work properly. When I say
``addons/``, I mean the addons directory inside of the openFrameworks
repository you clone in step two below. The same goes for all paths
below.

* Install your LeapMotion device by visiting `their site <http://leapmotion.com/setup>`_.
* Clone the `openFrameworks repository <http://github.com/openframeworks/openFrameworks/>`_.
* Run the appropriate ``install_dependencies.sh`` script from ``scripts/linux/..``
* Clone `ofxFX <http://github.com/patriciogonzalezvivo/ofxFX>`_ into ``addons/ofxFX``
* Clone the ofxLeapMotion addon ``addons/ofxLeapMotion``.
* Clone `ofxPlaylist <https://github.com/tgfrerer/ofxPlaylist>`_ into ``addons/ofxPlaylist``.
* Clone other dependencies of this project, found in ``addons/ofxVisulove/VisuLove/addons.make``.
* Clone this repository into ``addons/ofxVisulove``
* Go into ``addons/ofxVisulove/visulove/`` and ``make visulove``, then ``bin/visulove``.
  Running ``make run`` or doing ``cd bin; ./visulove`` does not work because of bugs.


Contribution
============

Yes please! Whether you want to contribute new Roses, or work on the
infrastructure, you are very welcome! At some point, we'll hopefully
also have a website up where people can share Roses easily.

Roses
-----

Take a look at the Rose class and look at some implementations of it in
``ofxVisulove/src/roses`` to see how Roses can be implemented.

New roses should be placed under
``ofxVisulove/src/roses/author_name/rose_name``. If you make your own
Roses and want to publish them, try to make it easy to use them by git
cloning your repository into ``ofxVisulove/src/roses``.

If your Rose is going to depend on some ofxAddons, these have to
be added to the ``ofxVisulove/addon_config.mk`` ADDON_DEPENDENCIES
variable.

We would be super happy if you want to share anything you make with the
community! The more, the merrier :)

Framework
---------

Here are some things we want to get done:

* Save position per Page

* Specify a file format for playlists.
* Complete work on PlaylistManager, GUI for it, and so on.
* Settings management
* Dynamically loaded Roses that don't require recompilation of the entire program.
* Metadata format for Roses.
* Capture sound from the OS (e.g. piped in from the DJ's computer), fast-fourier-transform
  it; to provide Roses with the ability to react to beats, volume, etc via InputHandler.
* Recording input events for live looping, demo playback, etc. This will probably also need
  a testing framework of some kind to ensure it actually works.
* Windows port
* OSX port
* Per-rose vsync support.
* Multi-monitor capability
    * Duplicate a Rose over several screens
    * Stretch a Rose over several screens
    * Display playlist, debug info, etc on one screen and Roses on another.
* Turn playlists into tree-like graphs (with loops!)
* Clarify terms of for contributing non-code content. Preferably some CC license.

* Multiply in HSV, YUV
* Hue shifting
* Modifier keys for MIDI sliders
* Save config for video groups
* Kaleidoskop
* Shaders as movies
* Chroma key transparency
* Color dodge blending mode, blending mode choice

* Make it possible to have different sized textures in ofxFX
